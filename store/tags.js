import ApiService from '@/api';

export default {
  namespaced: true,
  state() {
    return {
      tagType: 'all',
      tags: []
    }
  },
  // return state
  getters: {
    getTagType(state) {
      return state.tagType
    }
  },
  // handle state and data
  mutations: {
    getTags(state, data) {
      state.tags = data
    },
    updateTagType(state, data) {
      state.tagType = data
    }
  },
  // commit mutations with data
  actions: {
    getTags(context) {
      ApiService.get('tags').then(response => {
        return context.commit('getTags', response.data.tags)
      })
    },
    updateTagType(context, tagType) {
      context.commit('updateTagType', tagType)
    }
  }
}
