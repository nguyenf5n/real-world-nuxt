import ApiService from '@/api'

export default {
  namespaced: true,
  state() {
    return {
      article: {
        author: {},
        body: '',
        createdAt: '',
        description: '',
        favorited: false,
        favoritesCount: 0,
        slug: '',
        tagList: [],
        title: '',
        updatedAt: ''
      },
      articles: {
        articles: [],
        articlesCount: null
      },
      userArticles: {
        articles: [],
        articlesCount: null
      },
      errors: null,
      comment: {
        author: {}
      },
      comments: []
    }
  },
  // return state
  getters: {
    getCurrentArticle(state) {
      return state.article
    }
  },
  // handle state and data
  mutations: {
    getArticles(state, data) {
      state.articles = data
    },
    setPages(state, data) {
      state.pages = data
    },
    setArticle(state, data) {
      state.article = data
    },
    setUserArticles(state, data) {
      state.userArticles = data
    },
    setErrors(state, errors) {
      state.errors = errors
    },
    setComments(state, comments) {
      state.comments = comments
    },
    setComment(state, comment) {
      state.comment = comment
    },
    setTagList(state, tag) {
      state.article.tagList.push(tag)
    },
    removeTagList(state, index) {
      state.article.tagList.splice(index, 1)
    }
  },
  // commit mutations with data
  actions: {
    getArticles(context, config) {
      return new Promise(resolve => {
        ApiService.query('articles', config).then(({ data }) => {
          context.commit('getArticles', data)
          resolve(data)
        })
      })
    },
    getArticleOfUser(context, config) {
      return new Promise(resolve => {
        ApiService.query('articles', config)
          .then(({ data }) => {
            context.commit('setUserArticles', data)
            resolve(data)
          })
          .catch(({ response }) => {
            context.commit('setErrors', response.data.errors)
          })
        })
    },
    getArticleFavoriteOfUser(context, config) {
      return new Promise(resolve => {
        ApiService.query('articles', config)
          .then(({ data }) => {
            context.commit('setUserArticles', data)
            resolve(data)
          })
          .catch(({ response }) => {
            context.commit('setErrors', response.data.errors)
          })
        })
    },
    createArticle(context, data) {
      let token = localStorage.getItem('login')
      if(token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.post('articles', { article: data })
            .then(({ data }) => {
              context.commit('setArticle', data.article)
              context.dispatch('getArticle', data.article.slug)
              resolve(data)
            })
            .catch(({ response }) => {
              context.commit('setErrors', response.data.errors)
            })
        })
      }
    },
    updateArticle(context, {article, slug}) {
      let token = localStorage.getItem('login')
      if(token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.put(`articles/${slug}`, { article: article })
            .then(({ data }) => {
              context.commit('setArticle', data.article)
              context.dispatch('getArticle', data.article.slug)
              resolve(data)
            })
            .catch(({ response }) => {
              context.commit('setErrors', response.data.errors)
            })
        })
      }
    },
    deleteArticle(context, data) {
      let token = localStorage.getItem('login')
      if(token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.delete(`articles/${data}`)
            .then(({ data }) => {
              context.commit('setArticle', data.article)
              resolve(data)
            })
        })
      }
    },
    getArticle(context, data) {
      return new Promise(resolve => {
        ApiService.get('articles', data)
          .then(({ data }) => {
            context.commit('setArticle', data.article)
            resolve(data)
          })
          .catch(({ response }) => {
            context.commit('setErrors', response.data.errors)
          })
      })
    },
    getComments(context, data) {
      return new Promise(resolve => {
        ApiService.get(`articles/${data}/comments`)
          .then(({ data }) => {
            context.commit('setComments', data.comments)
            resolve(data)
          })
          .catch(({ response }) => {
            context.commit('setErrors', response.data.errors)
          })
      })
    },
    postComment(context, { comment, slug }) {
      let token = localStorage.getItem('login')
          comment = Object.assign({}, comment)
      if(token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.post(`articles/${slug}/comments`, comment)
            .then(({ data }) => {
              context.commit('setComment', data.comment)
              context.dispatch('getComments', slug)
              resolve(data)
            })
            .catch(({ response }) => {
              context.commit('setErrors', response.data.errors)
            })
        })
      }
    },
    deleteComment(context, { slug, id }) {
      let token = localStorage.getItem('login')
      if(token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.delete(`articles/${slug}/comments/${id}`)
            .then(({ data }) => {
              context.dispatch('getComments', slug)
              resolve(data)
            })
            .catch(({ response }) => {
              context.commit('setErrors', response.data.errors)
            })
        })
      }
    },
    postFavoriteArticle(context, { slug, config, isFavorite, isProfileOrFav = null }) {
      if(isFavorite) {
        return new Promise(resolve => {
          ApiService.delete(`articles/${slug}/favorite`)
            .then(({ data }) => {
              context.dispatch('getArticles', config)
              if(isProfileOrFav) {
                if(isProfileOrFav === 'profile') {
                  context.dispatch('getArticleOfUser', config)
                } else {
                  context.dispatch('getArticleFavoriteOfUser', config)
                }
              }
              resolve(data)
            })
            .catch(({ response }) => {
              context.commit('setErrors', response.data.errors)
            })
        })
      }

      return new Promise(resolve => {
        ApiService.post(`articles/${slug}/favorite`)
          .then(({ data }) => {
            context.dispatch('getArticles', config)
            if(isProfileOrFav === 'profile') {
              context.dispatch('getArticleOfUser', config)
            } else {
              context.dispatch('getArticleFavoriteOfUser', config)
            }
            resolve(data)
          })
          .catch(({ response }) => {
            context.commit('setErrors', response.data.errors)
          })
      })
    },
    addTagList(context, tag) {
      context.commit('setTagList', tag)
    },
    removeTagList(context, tag) {
      context.commit('removeTagList', tag)
    }
  }
}
