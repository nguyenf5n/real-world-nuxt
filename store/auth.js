import ApiService from '@/api'

export default {
  namespaced: true,
  state() {
    return {
      errors: null,
      user: {},
      profile: {}
    }
  },
  // return state
  getters: {
    getCurrentUser(state) {
      return state.user
    },
    getCurrentProfile(state) {
      return state.profile
    }
  },
  // handle state and data
  mutations: {
    setError(state, errors) {
      state.errors = errors
    },
    setCurrentUser(state, user) {
      state.user = user
      state.errors = null
    },
    setCurrentProfile(state, profile) {
      state.profile = profile
    }
  },
  // commit mutations with data
  actions: {
    userLogin(context, data) {
      return new Promise(resolve => {
        ApiService.post('users/login', { user: data })
          .then(({ data }) => {
            context.commit('setCurrentUser', data.user)
            ApiService.saveToken(data.user.token)
            resolve(data)
          })
          .catch(({ response }) => {
            context.commit('setError', response.data.errors)
          })
      })
    },
    getUserLogin(context) {
      let token = localStorage.getItem('login')
      if (token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.query('user').then(({ data }) => {
            context.commit('setCurrentUser', data.user)
            resolve(data)
          })
        })
      }
    },
    userLogout(context) {
      localStorage.removeItem('login')
      context.commit('setCurrentUser', {})
    },
    registerMember(context, data) {
      return new Promise(resolve => {
        ApiService.post('users', { user: data })
          .then(({ data }) => {
            context.commit('setCurrentUser', data.user)
            ApiService.saveToken(data.user.token)
            resolve(data)
          })
          .catch(({ response }) => {
            context.commit('setError', response.data.errors)
          })
      })
    },
    updateUser(context, data) {
      let token = localStorage.getItem('login')
      if (token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.put('user', { user: data })
            .then(({ data }) => {
              context.commit('setCurrentUser', data.user)
              resolve(data)
            })
            .catch(({ response }) => {
              context.commit('setError', response.data.errors)
            })
        })
      }
    },
    getProfile(context, data) {
      return new Promise(resolve => {
        ApiService.get('profiles', data)
          .then(({ data }) => {
            context.commit('setCurrentProfile', data.profile)
            resolve(data)
          })
          .catch(({ response }) => {
            context.commit('setError', response.data.errors)
          })
      })
    },
    followUser(context, data) {
      let token = localStorage.getItem('login')
      if (token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.post(`profiles/${data}/follow`)
            .then(({ data }) => {
              context.commit('setCurrentProfile', data.profile)
              resolve(data)
            })
            .catch(({ response }) => {
              context.commit('setError', response.data.errors)
            })
        })
      }
    },
    unFollowUser(context, data) {
      let token = localStorage.getItem('login')
      if (token) {
        ApiService.setHeaderAuth(token)
        return new Promise(resolve => {
          ApiService.delete(`profiles/${data}/follow`)
            .then(({ data }) => {
              context.commit('setCurrentProfile', data.profile)
              resolve(data)
            })
            .catch(({ response }) => {
              context.commit('setError', response.data.errors)
            })
        })
      }
    }
  }
}
